import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AlertsComponent } from 'src/app/alerts/alerts.component';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-categories-manager',
  templateUrl: './categories-manager.component.html',
  styleUrls: ['./categories-manager.component.css']
})
export class CategoriesManagerComponent implements OnInit {

  @ViewChild("btnOpenAddEditModal", { static: false }) btnOpenAddEditModal: ElementRef;
  @ViewChild("btnCloseAddEditModal", { static: false }) btnCloseAddEditModal: ElementRef;
  @ViewChild("oAlert", { static: false }) oAlert: AlertsComponent;


  public oModel: any;
  public btnSearchLabel: string = "Search";
  public btnSave: string = "Save";

  constructor(private http: HttpClient) {


  }

  ngOnInit() {

    this.oAlert = new AlertsComponent();
    this.vGetEmptyModel();

  }

  public vGetEmptyModel() {

    this.http.get("http://localhost:51295/api/categories/CategoriesGetEmptyRequest").subscribe(
      data => {
        this.oModel = data;
        console.log("Here Is the Model");
        console.log(this.oModel);


      }
    );

  }

  public vSearchCats() {


    this.btnSearchLabel = "Searching..."
    this.http.post("http://localhost:51295/api/categories/CategoriesSearch", this.oModel).subscribe(
      data => {
        this.oModel = data;
        this.btnSearchLabel = "Search";
      }
    )
  }

  public vAddCat() {
    this.btnOpenAddEditModal.nativeElement.click();
  }

  public vSave(bIsValid: boolean) {
    if (bIsValid) {
      this.btnSave = "Saving...";
      this.vDoSaveOnServer();


    }
    else {
      //Display Error Msg Invalid Data
      this.oAlert.vShowError(true, true, "Invalid data.");
    }
  }

  public vEdit(oCat) {
    this.oModel.model = oCat;
    this.btnOpenAddEditModal.nativeElement.click();
  }

  public vDelete(oCat) {
    if (confirm("Delete this record?")) {
      this.oModel.model = oCat;

      this.http.post("http://localhost:51295/api/categories/CategoriesDelete", this.oModel)
        .subscribe(
          data => {
            this.oModel = data;

            if (this.oModel.opResultCode == 1) {
              //Display It Is Saved Successfully
              this.oAlert.vShowError(true, false, this.oModel.opResultMsg);


            }
            else {
              //Display Error Message
              this.oAlert.vShowError(true, true, this.oModel.opResultMsg);

            }
          },
          error => {
            this.oAlert.vShowError(true, true, "Technical Error.");

          }

        );

    }


  }

  public vToggleActiveStatus(oCat)
  {
     this.oModel.model = oCat;
     this.oModel.model.categoryIsActive = !this.oModel.model.categoryIsActive;
     this.vDoSaveOnServer();

  }

  private vDoSaveOnServer()
  {
    this.http.post("http://localhost:51295/api/categories/CategoriesSave", this.oModel).subscribe(
      data => {
        this.oModel = data;
        console.log(this.oModel);
        if (this.oModel.opResultCode == 1) {
          //Display It Is Saved Successfully
          this.oAlert.vShowError(true, false, this.oModel.opResultMsg);


        }
        else {
          //Display Error Message
          this.oAlert.vShowError(true, true, this.oModel.opResultMsg);

        }

        this.btnSave = "Save";
        this.btnCloseAddEditModal.nativeElement.click();
      }
    );
  }

}
