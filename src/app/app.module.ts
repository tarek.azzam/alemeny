import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AlertsComponent } from './alerts/alerts.component';
import { MyApiConfig } from './Models/MyApiConfig';
import { HomeComponent } from './home/home.component';
import { TeacherProfileComponent } from './Teacher/teacher-profile/teacher-profile.component';
import { TeacherRegistrationComponent } from './teacher/teacher-registration/teacher-registration.component';
import { UsersService } from './Services/Users/users-service.service';
import { AuthenticateService } from './Services/Authentication/authenticate.service';
import { AuthHelpers } from './Interceptors/AuthHelpers';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { TeacherDashboardComponent } from './teacher/teacher-dashboard/teacher-dashboard.component';
import { StudentDashboardComponent } from './student/student-dashboard/student-dashboard.component';
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';
import { ActivateAccountComponent } from './accounts/activate-account/activate-account.component';
import { AccountLoginComponent } from './accounts/account-login/account-login.component';
import { StudentsProfileComponent } from './student/students-profile/students-profile.component';
import { CategoriesManagerComponent } from './admin/categories-manager/categories-manager.component';


@NgModule({
  declarations: [
    AppComponent,
    AlertsComponent,
    HomeComponent,
    TeacherProfileComponent,
    TeacherRegistrationComponent,
    HeaderComponent,
    FooterComponent,
    TeacherDashboardComponent,
    StudentDashboardComponent,
    AdminDashboardComponent,
    ActivateAccountComponent,
    AccountLoginComponent,
    StudentsProfileComponent,
    CategoriesManagerComponent,
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule

  ],
  providers: [
    MyApiConfig,
    UsersService,
    AuthenticateService,
    {provide: HTTP_INTERCEPTORS, useClass:AuthHelpers, multi:true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
