import { Observable } from "rxjs";


export interface IBaseRequest
{
    Save():Observable<any>;
    Delete():Observable<any>;
    Search():Observable<any>;
    GetByID():Observable<any>;
    GetAll():Observable<any>;
}