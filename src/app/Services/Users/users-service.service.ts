import { Injectable } from '@angular/core';
import { IBaseRequest } from 'src/app/Abstracts/IBaseRequest';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { MyApiConfig } from 'src/app/Models/MyApiConfig';


@Injectable({
  providedIn: 'root'
})
export class UsersService implements IBaseRequest {

  constructor(private http:HttpClient, private oApiConfig: MyApiConfig) { }
  Save(): Observable<any> {
    throw new Error("Method not implemented.");
  }
  Delete(): Observable<any> {
    throw new Error("Method not implemented.");
  }
  Search(): Observable<any> {
    throw new Error("Method not implemented.");
  }
  GetByID(): Observable<any> {
    throw new Error("Method not implemented.");
  }
  GetAll(): Observable<any> {
    throw new Error("Method not implemented.");
  }
  
}
