import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MyApiConfig, enRoles } from 'src/app/Models/MyApiConfig';
import { Router } from '@angular/router';
import { Observable, observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthenticateService {
  @Output() onUserLogin: EventEmitter<boolean> = new EventEmitter<boolean>();
  
  constructor(private http: HttpClient, private oApiConfig: MyApiConfig, private router: Router) {

  }

  //Activate user account
  ActivateAccount(sHash: string): Observable<boolean> {
    return new Observable((observer) => {
      this.http.get<boolean>(this.oApiConfig.UsersActivateAccountURL + "?id=" + sHash)
        .subscribe(
          data => {
            observer.next(data);
          },
          error => {
            observer.error(error);
          }
        );

    });

  }

  //Get Login Object From Server
  GetEmptyLoginModel(): Observable<any> {
    return new Observable(
      (observer) => {
        this.http.get(this.oApiConfig.UsersGetEmptyRegModelURL).subscribe(
          data => {
            observer.next(data);
          },
          error => {
            observer.error(error);
          }
        );

      }
    );
  }

  //Users login
  Login(oModel: any): Observable<boolean> {

    return new Observable((observer) => {
      this.http.post(this.oApiConfig.UserLoginURL, oModel).subscribe(
        data => {
          oModel = data;
          if (oModel.opResultCode == 1) {
            sessionStorage.setItem("LoginUser", JSON.stringify(oModel));
            this.onUserLogin.emit(true);
            observer.next(true);
          }
          else {
            this.onUserLogin.emit(false);
            observer.next(false);
          }
        },
        error => {
          this.onUserLogin.emit(false);
          observer.error(error);
        }
      );
    }
    );

    
  }

  //Logout from client machine
  Logout(): Observable<boolean> {
    return new Observable<boolean>(
      (observer) => {
        sessionStorage.clear();
        this.onUserLogin.emit(false);
        observer.next(true);
      }
    );
  }

  //Redirect to home page of the logged in user type
  RedirectToUserHomePage(oModel: any) {
    console.log("model", oModel);
    switch (oModel.model.roleId) {
      case enRoles.SysAdmin:
        this.router.navigate(["admin-dashboard"]);
        break;
      case enRoles.Teacher:
        if (oModel.model.userFirstName == "" || oModel.model.userName == "") {
          this.router.navigate(["teacher-profile"]);
        }
        else {
          this.router.navigate(["teacher-dashboard"]);
        }

        break;
      case enRoles.Student:
        if (oModel.model.userFirstName == "" || oModel.model.userName == "") {
          this.router.navigate(["student-profile"]);
        }
        else {
          this.router.navigate(["student-dashboard"]);
        }

    }
  }

  //Check if user is logged in or not and if the token is valid or not then fire event for the user status
  ValidateUserToken() {
    if (sessionStorage.getItem("LoginUser") != null) {
      let oLoginUser = JSON.parse(sessionStorage.getItem("LoginUser"));
      if (oLoginUser.token != null) {
        this.http.get<boolean>(this.oApiConfig.UsersIsLoggedInURL).subscribe(
          data => {
            this.onUserLogin.emit(data);
          },
          error => {
            this.onUserLogin.emit(false);
          }
        );

      }
      else {
        this.onUserLogin.emit(false);
      }
    }
    else {
      this.onUserLogin.emit(false);
    }

  }

  //Gets login token from session storage
  GetLoginToken(): string {
    if (sessionStorage.getItem("LoginUser") != null) {
      let oLoginUser = JSON.parse(sessionStorage.getItem("LoginUser"));
      return oLoginUser.token;

    }
    else {
      return null;
    }
  }

  //Gets Logged In User Info from session storage
  GetLoggedUser() {
    if (sessionStorage.getItem("LoginUser") != null) {
      return JSON.parse(sessionStorage.getItem("LoginUser"));
    }
    return null;
  }


  //Check If User Is Logged In
  CheckIfUserLoggedIn(): Observable<boolean> {
    return new Observable(
      (observer) => {
        this.http.get<boolean>(this.oApiConfig.UsersIsLoggedInURL).subscribe(
          data => {
            observer.next(data);
          },
          error => {
            observer.next(false);
          }

        );
      }
    );
  }

  //Check If User Is Logged In
  CheckIfTeacherLoggedIn(): Observable<boolean> {
    return new Observable(
      (observer) => {
        this.http.get<boolean>(this.oApiConfig.TeacherIsLoggedInURL).subscribe(
          data => {
            observer.next(data);
          },
          error => {
            observer.next(false);
          }

        );
      }
    );
  }

  //Check If User Is Logged In
  CheckIfStudentLoggedIn(): Observable<boolean> {
    return new Observable(
      (observer) => {
        this.http.get<boolean>(this.oApiConfig.StudentIsLoggedInURL).subscribe(
          data => {
            observer.next(data);
          },
          error => {
            observer.next(false);
          }

        );
      }
    );
  }

  //Check If Admin Is Logged In
  CheckIfAdminLoggedIn(): Observable<boolean> {
    return new Observable(
      (observer) => {
        this.http.get<boolean>(this.oApiConfig.AdminIsLoggedInURL).subscribe(
          data => {
            observer.next(data);
          },
          error => {
            observer.next(false);
          }

        );
      }
    );
  }


}
