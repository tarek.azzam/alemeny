import { Injectable } from '@angular/core';

@Injectable()
export class MyApiConfig
{
    public baseURL:string = "http://localhost:51295/";
    public sAPILocation:string = "api/";

    

    public UsersGetEmptyRegModelURL:string = this.baseURL + this.sAPILocation + "users/UsersGetEmptyRegModel";
    public UserEmailIsValidURL:string = this.baseURL + this.sAPILocation + "users/UserEmailIsValid";
    public UsersRegisterURL:string = this.baseURL + this.sAPILocation + "users/UsersRegister";
    public UserLoginURL:string = this.baseURL + this.sAPILocation + "users/UserLogin";
    public TestProtectTeacherURL:string = this.baseURL + this.sAPILocation + "users/TestProtectTeacher";
    public TestProtectStudentURL:string = this.baseURL + this.sAPILocation + "users/TestProtectStudent";
    public UsersIsLoggedInURL:string = this.baseURL + this.sAPILocation + "users/UsersIsLoggedIn";
    public TeacherIsLoggedInURL:string = this.baseURL + this.sAPILocation + "users/TeacherIsLoggedIn";
    public StudentIsLoggedInURL:string = this.baseURL + this.sAPILocation + "users/StudentIsLoggedIn";
    public AdminIsLoggedInURL:string = this.baseURL + this.sAPILocation + "users/AdminIsLoggedIn";
    public UsersActivateAccountURL: string = this.baseURL + this.sAPILocation + "users/UsersActivateAccount";


}
export enum enRoles{
    SysAdmin=1,
    Teacher,
    Student

}
