import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { TeacherRegistrationComponent } from './teacher/teacher-registration/teacher-registration.component';
import { TeacherProfileComponent } from './Teacher/teacher-profile/teacher-profile.component';
import { TeacherDashboardComponent } from './teacher/teacher-dashboard/teacher-dashboard.component';
import { StudentDashboardComponent } from './student/student-dashboard/student-dashboard.component';
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';
import { AccountLoginComponent } from './accounts/account-login/account-login.component';
import { ActivateAccountComponent } from './accounts/activate-account/activate-account.component';
import { TeacherGuardGuard } from './Guards/teacher-guard.guard';
import { StudentsProfileComponent } from './student/students-profile/students-profile.component';
import { CategoriesManagerComponent } from './admin/categories-manager/categories-manager.component';




const routes: Routes = [
  { path: '', redirectTo:'/account-login', pathMatch:'full' },
  { path: 'manage-cats', component: CategoriesManagerComponent },
  { path: 'teacher-register', component: TeacherRegistrationComponent, canActivate:[TeacherGuardGuard] },
  { path: 'teacher-profile', component: TeacherProfileComponent, canActivate:[TeacherGuardGuard] },
  { path: 'teacher-dashboard', component: TeacherDashboardComponent, canActivate:[TeacherGuardGuard]  },
  { path: 'student-dashboard', component: StudentDashboardComponent },
  { path: 'student-profile', component: StudentsProfileComponent },
  { path: 'admin-dashboard', component: AdminDashboardComponent },
  { path: 'account-login', component: AccountLoginComponent },
  { path: 'activate-account/:id', component: ActivateAccountComponent },
  { path: 'home', component: HomeComponent },
  { path: '**', redirectTo:'/account-login'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
