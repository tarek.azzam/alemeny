import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.css']
})
export class AlertsComponent implements OnInit {

  public bIsError:boolean = false;
  public sMsg:string ="";
  public bShowMsg:boolean = false;

  constructor() { }

  public vShowError(bShowMsg:boolean, bIsError:boolean, sMsg:string)
  {
    this.bIsError = bIsError;
    this.bShowMsg = bShowMsg;
    this.sMsg = sMsg;

  }
  public vClose()
  {
    this.sMsg = "";
    this.bShowMsg = false;
  }

  ngOnInit() {
  }

}
