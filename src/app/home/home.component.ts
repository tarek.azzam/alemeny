import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertsComponent } from '../alerts/alerts.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MyApiConfig } from '../Models/MyApiConfig';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  @ViewChild("oAlert", { static: false }) oAlert: AlertsComponent;

  public oLoginUserRequest:any;
  public oModel: any;
  public bIsButtonDisabled:boolean = false;
  public sSubmitLabel:string = "Login";
  
  //private authHeaders: any;

  constructor(private http: HttpClient, private oApiConfig: MyApiConfig) { 

  }

   ngOnInit() {
    this.oAlert = new AlertsComponent();
    this.oLoginUserRequest = JSON.parse(sessionStorage.getItem("LoginUser"));

    // this.authHeaders = new HttpHeaders({
    //   'Content-Type': 'application/json',
    //   'Authorization': this.oLoginUserRequest.token
    // });

   
  }

  public vTestProtectTeacher() {
    this.http.get(this.oApiConfig.TestProtectTeacherURL).subscribe(
      data => {
        this.oModel = data;
        console.log("Here Is the Model");
        console.log(this.oModel);


      },
      error =>
      {
        console.log("The error");
        console.log(error);
      }
    );
  }

  public TestProtectStudent() {
    this.http.get(this.oApiConfig.TestProtectStudentURL).subscribe(
      data => {
        this.oModel = data;
        console.log("Here Is the Model");
        console.log(this.oModel);


      },
      error =>
      {
        console.log("The error");
        console.log(error.status + " " + error.statusText);
      }
    );
  }

}
