import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertsComponent } from 'src/app/alerts/alerts.component';
import { ThrowStmt } from '@angular/compiler';
import { AuthenticateService } from 'src/app/Services/Authentication/authenticate.service';

@Component({
  selector: 'app-activate-account',
  templateUrl: './activate-account.component.html',
  styleUrls: ['./activate-account.component.css']
})
export class ActivateAccountComponent implements OnInit {

  @ViewChild("oAlert", { static: false }) oAlert: AlertsComponent;
  private sHash: string = '';

  constructor(private activeRoute: ActivatedRoute, private oAuthService: AuthenticateService, private router: Router) { }

  ngOnInit() {
    this.vActivateAccount();
  }
  private vActivateAccount() {
    if (this.activeRoute.snapshot.paramMap.get("id") == null) {
      this.oAlert.vShowError(true, true, "You are not authorized to access this page.");
   }
    else {
      this.sHash = this.activeRoute.snapshot.paramMap.get("id");
      if (this.sHash != null) {
        this.oAuthService.ActivateAccount(this.sHash).subscribe(
          data => {

            if (data) {
              this.oAlert.vShowError(true, false, "Account activated, redirecting to login page...");
              setTimeout(() => {
                this.router.navigate(["/account-login"]);

              }, 1000);
            } else {
              this.oAlert.vShowError(true, true, "This is not a valid account.");
            }

          },
          error => {
            this.oAlert.vShowError(true, true, "Technical error.");
          }
        );
      }
      else {
        this.oAlert.vShowError(true, true, "You are not authorized to access this page.");
      }
    }
  }
}
