import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertsComponent } from 'src/app/alerts/alerts.component';
import { HttpClient } from '@angular/common/http';
import { MyApiConfig } from 'src/app/Models/MyApiConfig';
import { AuthenticateService } from 'src/app/Services/Authentication/authenticate.service';

@Component({
  selector: 'app-account-login',
  templateUrl: './account-login.component.html',
  styleUrls: ['./account-login.component.css']
})
export class AccountLoginComponent implements OnInit {

  @ViewChild("oAlert", { static: false }) oAlert: AlertsComponent;

  public oModel: any;
  public bIsButtonDisabled: boolean = false;
  public sSubmitLabel: string = "Login";
  constructor(private oAuthService: AuthenticateService) {

  }

  ngOnInit() {
    this.oAlert = new AlertsComponent();
    this.vGetEmptyModel();
  }

  // Get Empty Login Model Object
  public vGetEmptyModel() {
    this.oAlert.vShowError(false, false, "");
    this.oAuthService.GetEmptyLoginModel().subscribe(
      data=>{
        this.oModel = data;

      },
      error=>
      {
        this.oAlert.vShowError(true, true, "Technical error.");
      }
    );
    
  }

  // User Login
  public vLogin(bIsValid: boolean) {
    this.oAlert.vShowError(false, false, "");
    if (bIsValid) {
      this.bIsButtonDisabled = true;
      this.sSubmitLabel = "Login..";

      this.oAuthService.Login(this.oModel).subscribe(
        data => {
          if (!data) {
            this.oAlert.vShowError(true, true, "Login failed.");
          }
          else {
            this.oAuthService.RedirectToUserHomePage(this.oAuthService.GetLoggedUser());
          }
          this.bIsButtonDisabled = false;
          this.sSubmitLabel = "Login";

        },
        error => {
          this.oAlert.vShowError(true, true, "Technical error.");
          this.bIsButtonDisabled = false;
          this.sSubmitLabel = "Login";

        }
      );

    }
    else {
      this.oAlert.vShowError(true, true, "Invalid data.");
      this.bIsButtonDisabled = false;
      this.sSubmitLabel = "Login";
    }
  }

  //Fetch empty request model
  public vFetchModel()
  {
    this.vGetEmptyModel();

  }


}
