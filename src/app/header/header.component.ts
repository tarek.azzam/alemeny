import { Component, OnInit } from '@angular/core';
import { AuthenticateService } from '../Services/Authentication/authenticate.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public bIsUserLoggedIn: boolean = false;

  constructor(private oAuthService: AuthenticateService, private router: Router) {
    oAuthService.ValidateUserToken();
  }

  ngOnInit() {
    this.oAuthService.onUserLogin.subscribe(bIsLoggedIn => {
      this.bIsUserLoggedIn = bIsLoggedIn;
      console.log("User Status", this.bIsUserLoggedIn);
    });
  }

  vLogout() {
    this.oAuthService.Logout()
      .subscribe(
        data => {
          console.log("stat", data)
          if (data) {
            this.router.navigate(["/account-login"]);
          }

        },
        error => {

        }
      );
  }

}
