import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertsComponent } from 'src/app/alerts/alerts.component';
import { HttpClient } from '@angular/common/http';
import { MyApiConfig, enRoles } from 'src/app/Models/MyApiConfig';

@Component({
  selector: 'app-teacher-registration',
  templateUrl: './teacher-registration.component.html',
  styleUrls: ['./teacher-registration.component.css']
})
export class TeacherRegistrationComponent implements OnInit {

  @ViewChild("oAlert", { static: false }) oAlert: AlertsComponent;

  public oModel: any;
  public bIsMailValid: boolean = true;
  public sConfirmEmail: string = '';
  public bIsButtonDisabled: boolean = false;
  public sSubmitLabel: string = "Submit";

  constructor(private http: HttpClient, private oApiConfig: MyApiConfig) {

  }

  ngOnInit() {
    this.oAlert = new AlertsComponent();
    this.vGetEmptyModel();
  }

  public vGetEmptyModel() {
    this.http.get(this.oApiConfig.UsersGetEmptyRegModelURL).subscribe(
      data => {
        this.oModel = data;
        console.log("Here Is the Model");
        console.log(this.oModel);


      }
    );
  }

  public vCheckIfMailValid() {
    this.http.post<boolean>(this.oApiConfig.UserEmailIsValidURL, this.oModel).subscribe(
      data => {

        console.log("Email is valid ");
        console.log(data);
        this.bIsMailValid = data;


      },
      error => {
        this.bIsMailValid = false;
        this.oAlert.vShowError(true, true, "Technical error try again later.");
      }
    );

  }

  public vRegister(bIsFormValid: boolean) {
    this.oAlert.vShowError(false, false, "");
    if (bIsFormValid) {

      this.bIsButtonDisabled = true;
      this.sSubmitLabel = "Submitting...";

      this.oModel.model.roleId = enRoles.Teacher;
      this.http.post(this.oApiConfig.UsersRegisterURL, this.oModel)
        .subscribe(
          data => {
            this.oModel = data;
            console.log("empty model");
            console.log(this.oModel);
            if (this.oModel.opResultCode == 1) {
              this.oAlert.vShowError(true, false, this.oModel.opResultMsg);
            }
            else {
              this.oAlert.vShowError(true, true, this.oModel.opResultMsg);
            }

            this.bIsButtonDisabled = false;
            this.sSubmitLabel = "Submit";

          },
          error => {
            this.oAlert.vShowError(true, true, "Technical error.");
            this.bIsButtonDisabled = false;
            this.sSubmitLabel = "Submit";
          }
        );

    }
    else {
      this.oAlert.vShowError(true, true, "Invalid data.");
    }

  }

}
