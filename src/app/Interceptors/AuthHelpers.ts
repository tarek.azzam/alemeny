import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticateService } from '../Services/Authentication/authenticate.service';

@Injectable()
export class AuthHelpers implements HttpInterceptor
{
    constructor(private AuthService:AuthenticateService){

    }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let sToken:string = this.AuthService.GetLoginToken();
        if(sToken == null)
        {
            sToken = '';
        }
       
        req = req.clone({
            setHeaders:
            {
                'Content-Type': 'application/json',
                Authorization:  sToken
            }

        });
        return next.handle(req);
    }
    
}